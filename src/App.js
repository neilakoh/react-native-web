import React, { Component } from 'react';
import { View, Text, Dimensions } from 'react-native';
import logo from './logo.svg';

const { height, width } = Dimensions.get('window');

class App extends Component {
  render() {
    return (
      <View style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'red',
        height: height
      }}>
        <View className="App-header">
          <Text>Sample is amazing</Text>
        </View>
      </View>
    );
  }
}

export default App;
