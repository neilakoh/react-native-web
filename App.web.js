import React from 'react';
import HybridApp from './src/App';
import { AppRegistry } from 'react-native';

AppRegistry.registerComponent('App', () => HybridApp);
